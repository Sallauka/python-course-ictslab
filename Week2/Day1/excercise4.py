import random
p1=0
p2=0

a={"rock": "scissors", "scissors": "paper", "paper": "rock"}
b=list(a)

while p1<5 and p2<5:

    x=random.choice(b)
    y=random.choice(b)
    if a[x]==y:
        print("P1 wins.",x,"beat",y)
        p1+=1
    elif a[y]==x:
        print("P2 wins.",y,"beat",x)
        p2+=1
    else:
        print("It's a tie.", x,"and",y)
if p1>p2:
    print("P1 wins overall: ",p1,":",p2)
else:
    print("P2 wins overall: ",p2,":",p1)

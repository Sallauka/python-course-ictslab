import random
p1=0
p2=0

a=["rock","paper","scissors"]

while p1<5 and p2<5:
    x=random.choice(a)
    y=random.choice(a)
    if x==y:
        print("It's a tie.")
        continue
    if x=="rock":
        if y=="scissors":
            p1+=1
            print("P1 wins. Rock beats scissors")
        else:
            p2+=1
            print("P2 wins. Paper beats rock")
    elif x=="paper":
        if y=="rock":
            p1+=1
            print("P1 wins. Paper beats rock")
        else:
            p2+=1
            print("P2 wins. Scissors beat paper")
    else:
        if y=="paper":
            p1+=1
            print("P1 wins. Scissors beat paper")
        else:
            p2+=1
            print("P2 wins. Rock beat scissors")


if p1<p2:
    print("P2 wins: ",p2,":",p1)
else:
    print("P1 wins: ",p1,":",p2)


